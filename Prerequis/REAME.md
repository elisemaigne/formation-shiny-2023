Merci de vous êtes inscrits à la formation Shiny.

Les horaires seront 9h-17h et 9h-12h le vendredi.

Quelques infos pratiques avant le jour J.

Il y aura des ordinateurs à disposition dans la salle, ils seront déjà configurés pour la formation. 
Si vous voulez venir avec votre ordinateur perso c'est possible mais voici la liste des pré-requis dans ce cas :

- R >=4.2.0 sur votre machine
- Rstudio
- Les packages suivants installés :
    - shiny
    - rsconnect
    - DT
    - shinydashboard
    - htmltools
    - shinyWidgets
    - shinyjs
    - shinycssloaders
    - shinyjqui
    - ggplot2
    - plotly
    - leaflet
    - shinythemes 
    - renv 

La commande suivante doit vous renvoyer TRUE

```
all(sapply(c("shiny", "rsconnect", "DT", "shinydashboard", "htmltools", "shinyWidgets", "shinyjs", 
         "shinycssloaders", "shinyjqui", "ggplot2", "plotly", "leaflet", "shinythemes", "renv"), require, character.only=TRUE))
```

Vous pouvez aussi lancer un exemple de shiny avec la commande suivante qui doit vous lancer une application :

`shiny::runExample("01_hello")`

Si ça ne marche pas merci de nous contacter en amont de la formation.


Le second jour de formation vous développerez une application de A à Z. 

Si vous souhaitez prendre des données avec lesquelles vous êtes familier.es et un bout de code R qui produit une analyse (dans l'idéal simple et relativement rapide, par exemple un ou plusieurs graphique) à partir de ces données vous pouvez les apporter avec vous, vous pourrez développer une application à partir de ça. 

Si vous n'avez pas le temps ou que vous n'en avez pas sous la main on vous fournira des données et des idées d'application dont vous pourrez vous inspirer pour créer la votre. 


Elise Maigné & Pierre Neuvial
elise.maigne@inrae.fr
pierre.neuvial@math.univ-toulouse.fr
