# Formation Shiny pour la plateforme Genotoul biostat 

https://genotoul-biostat.pages.mia.inra.fr/website/


# Formation Shiny

## Jour 1 : les bases

Lien vers la présentation : https://elisemaigne.pages.mia.inra.fr/formation-shiny/1_les_bases.html

## Jour 2 : je construis mon application de A à Z

Lien vers la présentation : https://elisemaigne.pages.mia.inra.fr/formation-shiny/2_pimp_my_app.html

## Jour 3 : les bonnes pratiques

Lien vers la présentation : https://elisemaigne.pages.mia.inra.fr/formation-shiny/3_bonnes_pratiques.html

