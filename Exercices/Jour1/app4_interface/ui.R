# app4 = correction de l'exercice 3
# Exercice 4 à faire à partir du code ci-dessous :
# Maintenant on va jouer avec l'interface. 
# Mettre le graphique et le tableau dans deux onglets différents. 
# ?shiny::tabsetPanel

# Define UI for application that draws a histogram
shinyUI(fluidPage(

    # Application title
    titlePanel("Mon application avec mes données"),

    # Sidebar with a slider input for number of bins
    sidebarLayout(
        sidebarPanel(
            sliderInput("bins",
                        "Number of bins:",
                        min = 1,
                        max = 50,
                        value = 30),
            selectInput(
                inputId = "myVar",
                label = "Variable",
                choices = c("Nb_Programmes", "Nb_Annules", "Nb_Retard")
            ),
            textInput(
                inputId = "myTitle",
                label = "Titre du graphique",
                value = "Histogramme"
            ),
            sliderInput(
                inputId = "myYears",
                label = "Choix des années",
                min = 2011,
                max = 2017,
                value=2011,
                step=1,
                animate = TRUE
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            plotOutput("distPlot"),
            "Données brutes :",
            tableOutput("myTable")
        )
    )
))
