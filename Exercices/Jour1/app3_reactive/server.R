# app3 = correction de l'exercice 2
# Exercice 3 à faire à partir du code ci-dessous :
# Faire en sorte que le slider sur l'année impacte à la fois le graphique et le tableau de données. 
# En utilisant une seule fois la valeur du slider côté server !
# ?shiny::reactive

# Define server logic required to draw a histogram
shinyServer(function(input, output) {
    
    output$distPlot <- renderPlot({
        # generate bins based on input$bins from ui.R
        x    <- datatrains[datatrains$Annee == input$myYears, input$myVar]
        bins <- seq(min(x), max(x), length.out = input$bins + 1)
        
        # draw the histogram with the specified number of bins
        hist(x, breaks = bins, 
             col = 'darkgray', 
             border = 'white',
             xlab=input$myVar, 
             main=input$myTitle)
    
    })
    
    output$myTable <- renderTable({
        datatrains
    })
    
})
