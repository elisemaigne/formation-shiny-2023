library(shiny)

# Partie Ui du module
# Chargement d'un jeu de données, choix du header et d'un séparateur
# + affichage d'un preview du jeu de données
# Le module retourne data.frame uploadé
uploadDatasetUI <- function(id) {
    tagList(
      # ... A REMPLIR
    )
}

# Partie Server du module
uploadDatasetServer <- function(id, params) {
    moduleServer(id, function(input, output, session) {
        # A REMPLIR
    })
}

# Application de démo du module de chargement d'un jeu de données
upLoadDatasetApp <- function() {
    ui <- fluidPage(
        uploadDatasetUI("myfile")
    )
    server <- function(input, output, session) {
        uploadDatasetServer("myfile")
    }
    shinyApp(ui, server)  
}

# Lancement de l'application de démo
#upLoadDatasetApp()
