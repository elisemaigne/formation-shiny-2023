# Reprendre le code de votre application et y appliquer les principes de `bonnes pratiques`. 

# !!!!!!
# Si vous n'êtes pas familier des outils indiqués ci-dessous, je vous conseille de copier le dossier de votre application pour en garder une copie qui marche. 
# !!!!!!

# - nettoyage modularité code
# - activation de renv
# - attention aux dépendances
# - organisation du code en sous fichiers,voire comme un package

###### Pour l'utilisation de renv :

library(renv)
renv::init() # Il va réinstaller réinstaller les packages. 
renv::status()
renv::snapshot() # Le fichier renv.lock créé contient les versions de R, des packages et de leurs dépendances. 

# Pour en savoir plus sur le fonctionnement de renv : https://elisemaigne.pages.mia.inra.fr/2021_package_renv/presentation.html

###### Minimiser les dépendances :

# 1. Pour chaque fonction appelée hors base, il est judicieux d'utiliser la notation package::fonction()
# 2. Pour voir où sont appelées les dépendences : 
renv::dependencies()

###### Shiny app as a package

# 1. Tour le code dans un dossier R et les données dans un dossier data/

# 2. Transformer votre application en une function indépendante ;
library(shiny)
myApp <- function(...) {
    ui <- fluidPage(
        ...
    )
    server <- function(input, output, session) {
        ...
    }
    shinyApp(ui, server, ...)
}

# 3. Générer un fichier DESCRIPTION
library(usethis)
usethis::use_description() # Génère un fichier description à remplir. 

# 4. Supprimer tous les appels à source()
# Si votre application est bien structurée comme un package, tout est chargé automatiquement en
# lançant devtools::load_all()

# 5. Le lancement de l'application devient monPackage::myApp()

# https://mastering-shiny.org/scaling-packaging.html

